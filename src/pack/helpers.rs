use std::{
	fs,
	io,
	path::{Path, PathBuf},
};

use miette::SourceSpan;

use super::{MetaEntry, Version};

pub fn root_preset_dir(install_path: impl AsRef<Path>) -> PathBuf {
	install_path.as_ref().join("Keysight").join("Default presets").join("Standard")
}

pub fn root_asset_dir(install_path: impl AsRef<Path>) -> PathBuf {
	install_path.as_ref().join("Keysight").join("Default textures")
}

pub fn custom_preset_dir() -> PathBuf {
	data_local_dir().join("Keysight").join("Saved").join("Presets")
}

pub fn custom_asset_dir(random: bool) -> PathBuf {
	data_local_dir()
		.join("Keysight")
		.join("Saved")
		.join(if random { "Textures (randomizer enabled)" } else { "Textures" })
}

#[derive(Debug, thiserror::Error, miette::Diagnostic)]
pub enum ValidationError {
	#[error("hash contained invalid characters")]
	#[diagnostic(code(unpack::asset_validate::hash))]
	Hash {
		#[source_code]
		hash: String,
		#[label = "here"]
		chr:  SourceSpan,
	},

	#[error("name contained invalid characters")]
	#[diagnostic(code(unpack::asset_validate::name))]
	Name {
		#[source_code]
		name: String,
		#[label = "here"]
		chr:  SourceSpan,
	},

	#[error("extension contained invalid characters")]
	#[diagnostic(code(unpack::asset_validate::ext))]
	Extension {
		#[source_code]
		ext: String,
		#[label = "here"]
		chr: SourceSpan,
	},
}

pub fn validate_asset(v: MetaEntry) -> Result<MetaEntry, ValidationError> {
	if let Some((idx, _)) =
		v.hash.bytes().enumerate().find(|(_, v)| !matches!(v, b'0'..=b'9' | b'a'..=b'f'))
	{
		return Err(ValidationError::Hash {
			hash: v.hash.clone(),
			chr:  SourceSpan::new(idx.into(), 1.into()),
		});
	}

	if let Some((idx, _)) =
		v.name.bytes().enumerate().find(|(_, v)| matches!(v, b'.' | b'/' | b'\\'))
	{
		return Err(ValidationError::Name {
			name: v.name.clone(),
			chr:  SourceSpan::new(idx.into(), 1.into()),
		});
	}

	if let Some((idx, _)) = v
		.extension
		.bytes()
		.enumerate()
		.find(|(_, v)| !matches!(v, b'a'..=b'z' | b'A'..=b'Z' | b'0'..=b'9'))
	{
		return Err(ValidationError::Extension {
			ext: v.extension.clone(),
			chr: SourceSpan::new(idx.into(), 1.into()),
		});
	}

	Ok(v)
}

fn data_local_dir() -> PathBuf {
	#[cfg(feature = "proton-steam-comptime")]
	{
		std::path::PathBuf::from(env!("PROTON_PATH_OVR"))
	}
	#[cfg(not(feature = "proton-steam-comptime"))]
	{
		dirs::data_local_dir().unwrap()
	}
}

pub fn maybe_format_version(version: Option<Version>) -> String {
	if let Some(ver) = version {
		format!("{:#X}", ver)
	} else {
		String::from("Unknown")
	}
}

pub fn list_all_presets() -> io::Result<Vec<String>> {
	let mut presets = Vec::new();
	for f in fs::read_dir(self::custom_preset_dir())? {
		let file = f?;
		if file.metadata()?.is_file() {
			if let Some(name) = file.path().file_stem() {
				presets.push(name.to_string_lossy().to_string());
			}
		}
	}

	Ok(presets)
}
