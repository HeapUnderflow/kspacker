use std::{fs::File, path::Path};

use chrono::{DateTime, Utc};

pub mod helpers;
pub(self) mod ks_preset;
pub mod packer;
pub mod unpacker;

pub type Version = u32;

#[derive(miette::Diagnostic, Debug, thiserror::Error)]
#[error("failed to load the keysight version")]
#[diagnostic(code(loader::keysight::version))]
pub enum KSVersionError {
	#[error("the default plain preset could not be found")]
	NotFound,
	#[error("the default plain preset could not be opened")]
	Open(#[source] std::io::Error),
	#[error("the default plain preset could not be parsed")]
	Parse(#[source] serde_json::Error),
}

pub fn get_ks_version(root: impl AsRef<Path>) -> Result<Version, KSVersionError> {
	let path = helpers::root_preset_dir(root).join("Plain (default).json");
	if !path.exists() {
		return Err(KSVersionError::NotFound);
	}

	#[derive(serde::Deserialize)]
	struct KSPresetVersion {
		#[serde(rename = "versionForUpdatePurposes")]
		pub version_for_update_purposes: Version,
	}

	let data: KSPresetVersion =
		serde_json::from_reader(File::open(path).map_err(KSVersionError::Open)?)
			.map_err(KSVersionError::Parse)?;

	Ok(data.version_for_update_purposes)
}

/// The metadata for the zip file
#[derive(Debug, Clone, Eq, PartialEq, serde::Serialize, serde::Deserialize)]
pub struct PackMetaData {
	pub name:           String,
	pub author:         String,
	pub description:    String,
	pub packed:         DateTime<Utc>,
	pub preset_version: Version,
	pub target_version: Version,

	pub assets: Vec<MetaEntry>,
}

/// A single asset packed
#[derive(Debug, Clone, Eq, PartialEq, serde::Serialize, serde::Deserialize)]
pub struct MetaEntry {
	pub hash:              String,
	pub name:              String,
	pub extension:         String,
	pub texture_type:      TextureType,
	pub source_was_random: bool,
}

/// Describes a Texture source for the given type
#[derive(Debug, Clone, Copy, Eq, PartialEq, Hash, serde::Serialize, serde::Deserialize)]
pub enum TextureType {
	Diffuse,
	Emissive,
	WorldStencil,
	Mask,
	Metalness,
	Normal,
	Roughness,
	Shape,
	Specular,
	Stencil,
	Reflection,
}

impl TextureType {
	pub fn path_name(&self) -> &'static str {
		match *self {
			TextureType::Diffuse | TextureType::Emissive => "Colour",
			TextureType::WorldStencil | TextureType::Mask => "Mask",
			TextureType::Metalness => "Metal",
			TextureType::Normal => "Normal",
			TextureType::Roughness => "Roughness",
			TextureType::Shape => "Particle stencil",
			TextureType::Specular => "Specular",
			TextureType::Stencil => "Pulse stencil",
			TextureType::Reflection => "Reflection",
		}
	}
}
